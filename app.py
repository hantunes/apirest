from flask import Flask
from flask import jsonify
from flask import request
from marshmallow import ValidationError
from db import db
from ma import ma
from models.artist import ArtistModel
from models.artist import ArtistSchema


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

artist_schema = ArtistSchema(strict=True)
artists_schema = ArtistSchema(strict=True, many=True)

@app.route('/artists/')
def get_artists():
    all_artists = ArtistModel.query.all()
    artists_list_schema = artists_schema.dump(all_artists)
    return jsonify({"artists": artists_list_schema.data}), 200

@app.route('/artist/<string:name>')
def get_artist(name):
    artist = ArtistModel.find_by_name(name)
    if not artist:
        return jsonify({"message": "artist not found"}), 404
    return jsonify(artist_schema.dump(artist).data), 200

@app.route('/artist/<int:artist_id>')
def get_artist_by_id(artist_id):
    artist = ArtistModel.find_by_id(artist_id)
    if not artist:
        return jsonify({"message": "artist not found"}), 404
    return jsonify(artist_schema.dump(artist).data), 200

@app.route('/artist', methods=['POST'])
def create_artist():
  
    request_data = request.get_json()
    try:
        artist = artist_schema.load(request_data)
    except ValidationError as err:
        errors = err.messages
        return jsonify({"message": errors}), 400
    artist.data.save_to_db()
    return jsonify(artist_schema.dump(request_data).data), 201

@app.route('/artist/<artist_id>', methods=['PUT'])
def set_artist_name(artist_id):
    request_data = request.get_json()
    try:
        artist = ArtistModel.find_by_id(artist_id)
    except ValidationError as err:
        errors = err.messages
        return jsonify({"message": errors}), 400
    
    artist.name = request_data.get('name')
    artist.save_to_db()
    return jsonify({"message": "Updated with successfull!"}), 200

@app.route('/artist/<string:name>', methods=['DELETE'])
def delete_artist(name):
  
    request_data = request.get_json()
    try:
        artist = ArtistModel.find_by_name(name)
    except ValidationError as err:
        errors = err.messages
        return jsonify({"message": errors}), 400
    artist.delete_from_db()
    return jsonify({"message": "Deleted with successfull!"}), 200

if __name__ == "__main__":
    db.init_app(app)
    ma.init_app(app)

    @app.before_first_request
    def create_tables():
        db.create_all()

    app.run(port=5000, debug=True)
